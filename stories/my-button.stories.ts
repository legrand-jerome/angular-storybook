import { Meta, Story } from '@storybook/angular';

import {MyButtonComponent} from '../projects/components/src/lib/my-button/my-button.component';

export default {
  title: 'OPAL/MyButtonComponent',
  component: MyButtonComponent,
  argTypes: {
    onClick: {action: 'clicked'},
  }
} as Meta;

const Template: Story<MyButtonComponent> = (args: MyButtonComponent) => ({props: args});

export const Primary = Template.bind({});
Primary.args = {
  label: 'MyButtonP',
  variant: 'primary',
};
Primary.storyName = '1 Primary';

export const Secondary = Template.bind({});
Secondary.args = {
  label: 'MyButton',
  variant: 'secondary',
};
Secondary.storyName = '2 Secondary';

export const Disabled = Template.bind({});
Disabled.args = {
  label: 'Disabled button',
  variant: 'primary',
  disabled: true
};
Disabled.storyName = '3 Disabled';
