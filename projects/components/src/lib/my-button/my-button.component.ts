import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'lib-my-button',
  templateUrl: './my-button.component.html',
  styleUrls: ['./my-button.component.scss']
})
export class MyButtonComponent {
  @Input() label: string = '';
  @Input() variant: 'primary'|'secondary' = 'primary';
  @Input() disabled = false;

  @Output() onClick = new EventEmitter<MouseEvent>();

  click(event: MouseEvent) {
    this.onClick.emit(event);
  }
}
